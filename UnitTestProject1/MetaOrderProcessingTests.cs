﻿using ConsoleApp2;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;

namespace UnitTestProject1
{
    [TestClass]
    public class MetaOrderProcessingTests : OrderProcessingBase
    {
        private IOrderProcessor _testSubject;


        [TestInitialize]
        public void Initialize()
        {
            _testSubject = new MetaOrderProcessing();
        }


        
        [TestMethod]
        public void TestOpenOrdersAreReturned()
        {
            var goodOrder = new Order { OrderNumber = 5, Status = OrderStatus.Open };
            var badOrder = new Order { OrderNumber = 6, Status = OrderStatus.Closed };

            var stopwatch = Stopwatch.StartNew();

            var testResult = _testSubject.GetOpenOrders(new List<Order> { goodOrder, badOrder }).ToList();

            stopwatch.Stop();

            Assert.IsTrue(testResult.Contains(goodOrder));
            Assert.IsFalse(testResult.Contains(badOrder));

            MethodBase m = MethodBase.GetCurrentMethod();
            WriteMessage($"Executing {m.ReflectedType.Name}.{m.Name} took {stopwatch.ElapsedTicks} ticks.");

        }

        [TestMethod]
        public void TestImportantOrdersAreFirst()
        {
            var inputOrders = CreateTestOrders();

            var stopwatch = Stopwatch.StartNew();
            var testResult = _testSubject.GetOrdersByMostImportant(inputOrders).ToList();
            stopwatch.Stop();

            PerformAssertions(inputOrders, testResult);

            MethodBase m = MethodBase.GetCurrentMethod();
            WriteMessage($"Executing {m.ReflectedType.Name}.{m.Name} took {stopwatch.ElapsedTicks} ticks.");

        }


        [TestMethod]
        public void TestPerformance()
        {
            var inputOrders = CreateTestOrders();

            var stopwatch = Stopwatch.StartNew();
            var testResult = _testSubject.GetOrdersByMostImportant(_testSubject.GetOpenOrders(inputOrders)).ToList();

            stopwatch.Stop();

            MethodBase m = MethodBase.GetCurrentMethod();
            WriteMessage($"Executing {m.ReflectedType.Name}.{m.Name} took {stopwatch.ElapsedTicks} ticks.");
        }
    }
}
