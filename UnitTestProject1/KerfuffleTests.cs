using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApp2;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Diagnostics;
using System.Reflection;

namespace UnitTestProject1
{
    [TestClass]
    public class KerfuffleTests : OrderProcessingBase
    {
        private Kerfuffle _testSubject;


        [TestInitialize]
        public void Initialize()
        {
            _testSubject = new Kerfuffle();
        }

        [TestMethod]
        public void TestOpenOrdersAreReturned()
        {
            var goodOrder = new Order { OrderNumber = 5, Status = OrderStatus.Open };
            var badOrder = new Order { OrderNumber = 6, Status = OrderStatus.Closed };

            Stopwatch stopwatch = Stopwatch.StartNew();

            var testResult = _testSubject.GetOpenOrders(new List<Order> { goodOrder, badOrder }).ToList();

            stopwatch.Stop();
            
            Assert.IsTrue(testResult.Contains(goodOrder));
            Assert.IsFalse(testResult.Contains(badOrder));

            MethodBase m = MethodBase.GetCurrentMethod();
            WriteMessage($"Executing {m.ReflectedType.Name}.{m.Name} took {stopwatch.ElapsedTicks} ticks.");
        }

        

        [TestMethod]
        public void TestImportantOrdersAreFirst()
        {
            var inputOrders = CreateTestOrders();

            var stopwatch = Stopwatch.StartNew();
            var testResult = _testSubject.GetOrdersByMostImportant(inputOrders).ToList();
            stopwatch.Stop();
            PerformAssertions(inputOrders, testResult);

            MethodBase m = MethodBase.GetCurrentMethod();
            WriteMessage($"Executing {m.ReflectedType.Name}.{m.Name} took {stopwatch.ElapsedTicks} ticks.");


        }

        [TestMethod]
        public void TestPerformance()
        {
            var inputOrders = CreateTestOrders();

            var stopwatch = Stopwatch.StartNew();
            var testResult = _testSubject.GetOrdersByMostImportant(_testSubject.GetOpenOrders(inputOrders)).ToList();
            stopwatch.Stop();

            MethodBase m = MethodBase.GetCurrentMethod();
            WriteMessage($"Executing {m.ReflectedType.Name}.{m.Name} took {stopwatch.ElapsedTicks} ticks.");

        }




    }
}
