﻿using ConsoleApp2;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace UnitTestProject1
{
    public class OrderProcessingBase
    {
        private string _curDirectory;

        public OrderProcessingBase()
        {
            _curDirectory = Directory.GetCurrentDirectory();
        }


        const int DUMNMY_ORDERS = 50000;
        //const int DUMNMY_ORDERS = 50 ;

        protected List<Order> CreateTestOrders()
        {

            return Enumerable.Range(11, DUMNMY_ORDERS)
                .Select(c => new Order
                {
                    OrderNumber = c,
                    Status = c % 2 == 0 ? OrderStatus.Open : OrderStatus.Closed,
                    Cust = new Customer { ID = 100 * c, LoyaltyScore = 1 }
                }).Concat(
                new List<Order>
            {
                new Order { OrderNumber = 5, Cust = new Customer { ID = 1, LoyaltyScore = 500 } },
                new Order { OrderNumber = 6, Cust = new Customer { ID = 2, LoyaltyScore = 600 } },
                new Order { OrderNumber = 7, Cust = new Customer { ID = 3, LoyaltyScore = 550 } },
                new Order { OrderNumber = 8, Cust = new Customer { ID = 4, LoyaltyScore = 1000 } },
                new Order { OrderNumber = 9, Cust = new Customer { ID = 5, LoyaltyScore = 9000 } },
                new Order { OrderNumber = 10, Cust = new Customer { ID = 6, LoyaltyScore = 5 } }
            }
                ).ToList();
        }
        protected void PerformAssertions(List<Order> inputOrders, List<Order> testResult)
        {
            Assert.AreEqual(inputOrders.Single(c => c.OrderNumber == 9), testResult[0], "0");
            Assert.AreEqual(inputOrders.Single(c => c.OrderNumber == 8), testResult[1], "1");
            Assert.AreEqual(inputOrders.Single(c => c.OrderNumber == 6), testResult[2], "2");
            Assert.AreEqual(inputOrders.Single(c => c.OrderNumber == 7), testResult[3], "3");
            Assert.AreEqual(inputOrders.Single(c => c.OrderNumber == 5), testResult[4], "4");
            Assert.AreEqual(inputOrders.Single(c => c.OrderNumber == 10), testResult[5], "5");
        }

        protected void WriteMessage(string message)
        {
            File.AppendAllText($@"{_curDirectory}\unit_test_output.txt", $"{message}\n");
        }
    }
}
