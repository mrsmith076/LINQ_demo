﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace ConsoleApp2
{
    public class LoyaltyComparer : IComparer<Order>
    {
        public int Compare([AllowNull] Order x, [AllowNull] Order y)
        {
            return y.Cust.LoyaltyScore - x.Cust.LoyaltyScore;
        }
    }
}
