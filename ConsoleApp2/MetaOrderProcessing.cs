﻿using System;
using System.Collections.Generic;

namespace ConsoleApp2
{
    public class MetaOrderProcessing : IOrderProcessor
    {
        public IEnumerable<Order> GetOpenOrders(IEnumerable<Order> orders)
        {
            var resultSet = new List<Order>();

            foreach (var order in orders)
            {
                if (order.Status == OrderStatus.Open)
                {
                    resultSet.Add(order);
                }
            }

            return resultSet;
        }

        public IEnumerable<Order> GetOrdersByMostImportant(IEnumerable<Order> orders)
        {
            var orderList = ArrayFromIEnumerable(orders);

            Array.Sort(orderList, new LoyaltyComparer());

            return orderList;
        }

        private Order[] ArrayFromIEnumerable(IEnumerable<Order> orders)
        {
            if (orders == null)
            {
                throw new ArgumentNullException(nameof(orders));
            }

            var enu = orders.GetEnumerator();

            long count = 0;
            int initialSize = 50;
            int incrementSize = 50;
            int currentSize = initialSize;
            var orderArray = new Order[initialSize];
            while (enu.MoveNext())
            {
                // check to see whether the count of elements is going to overflow the array
                if (count == currentSize)
                {
                    // make the array bigger
                    var swapArray = orderArray;

                    currentSize += incrementSize;
                    orderArray = new Order[currentSize];

                    swapArray.CopyTo(orderArray, 0);
                }
                orderArray[count++] = enu.Current;
            }

            var wrkArray = orderArray;
            orderArray = new Order[count];
            for (int i = 0; i < count; i++)
            {
                orderArray[i] = wrkArray[i];
            }
            return orderArray;
        }
    }
}
