﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ConsoleApp2
{
    public enum OrderStatus
    {
        Open,
        Closed,
        Cancelled
    }

    [DebuggerDisplay("Order - {OrderNumber}")]
    public class Order 
    {
        public int OrderNumber { get;  set; }
        public OrderStatus Status { get;  set; }

        public Customer Cust { get; set; }

        private string GetDebuggerDisplay()
        {
            return ToString();
        }

        public override string ToString()
        {
            return $"Order - {OrderNumber}";
        }
    }
}
