﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp2
{

    public class Kerfuffle : IOrderProcessor
    {

        public IEnumerable<Order> GetOpenOrders(IEnumerable<Order> orders)
        {
            return orders.Where(x => x.Status == OrderStatus.Open);
        }



        public IEnumerable<Order> GetOrdersByMostImportant(IEnumerable<Order> orders)
        {
            return orders.OrderByDescending(x => x.Cust.LoyaltyScore);

        }

        public IEnumerable<Order> GetImportantOpenOrders(IEnumerable<Order> orders)
        {
            return GetOrdersByMostImportant(GetOpenOrders(orders));
        }
    }
}
