﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    public interface IOrderProcessor
    {
        IEnumerable<Order> GetOpenOrders(IEnumerable<Order> orders);

        IEnumerable<Order> GetOrdersByMostImportant(IEnumerable<Order> orders);
    }
}
