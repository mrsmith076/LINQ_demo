﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    public class TraditionalOrderProcessor : IOrderProcessor
    {
        public IEnumerable<Order> GetOpenOrders(IEnumerable<Order> orders) { 
            var resultSet = new List<Order>();

            foreach (var order in orders)
            {
                if (order.Status == OrderStatus.Open)
                {
                    resultSet.Add(order);
                }
            }

            return resultSet;
        }
        public IEnumerable<Order> GetOrdersByMostImportant(IEnumerable<Order> orders)
        {

            var orderList = new List<Order>();
            foreach(var o in orders)
            {
                orderList.Add(o);
            }

            Order swapOrder = null;
            Func<Order, Order, bool> comp = (Order x, Order y) => x.Cust.LoyaltyScore < y.Cust.LoyaltyScore;

            for (int i = 0; i < orderList.Count; i++)
            {
                for (int j = i + 1; j < orderList.Count; j++)
                {
                    if (comp(orderList[i], orderList[j]))
                    {
                        swapOrder = orderList[i];
                        orderList[i] = orderList[j];
                        orderList[j] = swapOrder;
                    }
                }
            }

            return orderList;
        }
    }
}
